=====================
LASTPASS ACCT BACKEND
=====================

Set up your acct profile like so with your lastpass credentials::

    acct-backend:
        lastpass:
            username: user@example.com
            password: password
            designator: acct-provider-


You can now specify providers and profiles in your lastpass vault.

Put them in a group that starts with `acct-provider-` and ends with the provider name.

For example, an `acct-provider-vultr` group for an idem-vultr profile.

The `Name` field of the lastpass vault item will be the profile name.

The rest of the profile information should go in the notes section in a yaml style.

.. image:: lastpass.png
